//Imports
const functions = require('firebase-functions');
const admin = require('firebase-admin');

//Constants
const DIFFICULTY_RADIUS = {
    'low': 12,
    'medium': 20,
    'high': 50,
    'extreme': 80
};
const ACCURACY_RADIUS = {
  'low': 9,
  'medium': 6,
  'high': 3,
  'extreme': 1
};
const EARTH_RADIUS = 6371000;

//Initialize Firebase
admin.initializeApp();

/**
 * Generate the stashes search radius
 * @type {CloudFunction<DocumentSnapshot>}
 */
exports.generateStashRadius = functions.firestore.document('/stashes/{stashId}').onCreate((snap, context) => {
    const difficulty = snap.get("difficulty");
    const accuracy = snap.get("accuracy");
    const stash_coords = snap.get("coords");

    if (difficulty !== null && accuracy !== null && stash_coords !== null) {
        //Determine radius
        let radius = DIFFICULTY_RADIUS[difficulty];
        let accuracy_radius = ACCURACY_RADIUS[accuracy];

        //Determine stash coordinate offset
        let search_offset = {
            x: Math.random() * (radius - accuracy_radius),
            y: Math.random() * (radius - accuracy_radius)
        };

        //Build new Geopoint
        let geopoint = new admin.firestore.GeoPoint(
            stash_coords.latitude + (search_offset.y / EARTH_RADIUS) * (180 / Math.PI),
            stash_coords.longitude + (search_offset.x / EARTH_RADIUS) * (180 / Math.PI) / Math.cos(stash_coords.latitude * Math.PI / 180));

        return snap.ref.set({'search_radius': radius, 'search_coords': geopoint}, {merge: true});
    } else return null;

});
